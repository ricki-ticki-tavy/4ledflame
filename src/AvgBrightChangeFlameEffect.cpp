//
// Created by dsporykhin on 29.02.20.
//

#include "AvgBrightChangeFlameEffect.h"

AvgBrightChangeFlameEffect::AvgBrightChangeFlameEffect(EFlame *eflame) : AbstractFlameEffect( 0.3, eflame,
                                                                                   true) {

}

void AvgBrightChangeFlameEffect::applyEffect() {
    flame->stepCountToDo = 0;
    flame->normalBright = 190 + (flame->doubleRandom() * 60) - 30;
#ifdef CON_DEBUG
    Serial.println("AvgBrightChangeFlameEffect  " + String(flame->normalBright));
#endif
}