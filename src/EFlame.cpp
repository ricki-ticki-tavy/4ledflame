//
// Created by dsporykhin on 29.02.20.
//

#include "EFlame.h"

EFlame::EFlame() {
    effectCounter = 0;
    stepCountToDo = 0;
    totalEffectProbability = 0;
    probabilityChangeAction = 0.0001;
    brightMax = 250;
    normalBright = 190;
    speedChangeBrightMin = 4;
    speedChangeBrightMax = 10;


    for (int index = 0; index < 4; index++) {
        brightDelta[index] = 0;
        brights[index] = 0;
        pinMode(ledMatrix[index], OUTPUT);
    };

}
//---------------------------------------------------------------------------

double EFlame::doubleRandom() {
    long maxVal = 1200000000L;
    long val = random(maxVal);
    double d = val;
    return d / (double) (maxVal);
}
//---------------------------------------------------------------------------

void EFlame::appendEffect(AbstractFlameEffect *effect) {
    if (effectCounter < MAX_EFFECTS_COUNT) {
        effects[effectCounter++] = effect;
        // суммарная вероятность возникновения нового эффекта
        totalEffectProbability += effect->probability;;
    }
}
//---------------------------------------------------------------------------

double EFlame::doubleAbs(double src) {
    return src < 0 ? -src : src;
}
//---------------------------------------------------------------------------

void EFlame::hdSetLedBright(int index) {
    analogWrite(ledMatrix[index], brights[index]);
}
//---------------------------------------------------------------------------

void EFlame::applyNewEffect() {
    // берем некое ранодомное число, не превышающее суммарной вероянтности выпадения всех эффектов.
    double effectSelector = doubleRandom() * totalEffectProbability;
    int newEffectIndex = -1;

    // Все вероятности сложены, как бы, в единый столбик. Выпавшее число не юольше размера этого столбика. ищем в кого
    // в этом столбике попало наше число. Чем выше было значение вероятности для эффекта, тем с большей вероятностью
    // число попадет именно в его диапазон
    for (int index = 0; index < effectCounter; index++) {
        if (effects[index]->probability > effectSelector) {
            newEffectIndex = index;
            break;
        } else {
            effectSelector -= effects[index]->probability;
        }
    }

    if (newEffectIndex != -1) {
        // все нормально. Иначе и быть не должно, но проверка должна быть
        effects[newEffectIndex]->apply();
    }
}
//---------------------------------------------------------------------------

void EFlame::loop() {
#ifdef CON_DEBUG
    Serial.print(" loop scnt=" + String(stepCountToDo));
#endif
    if (stepCountToDo <= 0 || doubleRandom() <= probabilityChangeAction) {
        // меняем направление свечения и все такое
        if (stepCountToDo > 0    // именно так! если он больше, но мы тут, то сработала безусловная смена эффекта
            || doubleRandom() <= totalEffectProbability) {
            applyNewEffect();
        }
    } else {
        for (int index = 0; index < 4; index++) {
#ifdef CON_DEBUG
            Serial.print("   idx=" + String(index) + ", "
                         + "  Led" + String(index) + "=" + String(brights[index]) + "+" + String(brightDelta[index]) + "->");
#endif
            brights[index] += brightDelta[index];
            if (brights[index] < 0) {
                brights[index] = 0;
            } else if (brights[index] > 255) {
                brights[index] = 255;
            }
#ifdef CON_DEBUG
            Serial.print(String(brights[index]));
#endif
            hdSetLedBright(index);
        }
        stepCountToDo--;
    }
#ifdef CON_DEBUG
    Serial.println(".");
#endif
}
//---------------------------------------------------------------------------
