//
// Created by dsporykhin on 29.02.20.
//

#ifndef EFLAME328_EFLAME_H
#define EFLAME328_EFLAME_H


#include "AbstractFlameEffect.h"

// кол-во форм, поддерживаемых менеджером
#define MAX_EFFECTS_COUNT 10

//#define CON_DEBUG

class AbstractFlameEffect;

class EFlame {
public:
    // видеоэффекты
    AbstractFlameEffect* effects[MAX_EFFECTS_COUNT];

    // кол-во эффектов
    int effectCounter;

    // кол-во циклов до окончания действия эффекта
    int stepCountToDo;

    // Нормальная средняя яркость
    int normalBright;

    // максимальная яркость
    int brightMax;

    // минимальная яркость
    int brightMin;

    // массив дельт для яркостей 4-х светодиодов
    double brightDelta[4];

    // Максимальная скорость изменения яркости
    double speedChangeBrightMax;

    // Минимальная скорость изменения яркости
    double speedChangeBrightMin;

    // массив текущих яркостей
    double brights[4];

    // Вероятность смены текущего эффекта новым до ео завершения
    double probabilityChangeAction;

    // рандом от 0 до 1
    double doubleRandom();

    // абсолютное значение
    double doubleAbs(double src);

    // Добавление эффекта
    void appendEffect(AbstractFlameEffect* effect);

    // Рабочий цикл программы
    void loop();



    EFlame();

private:
    double totalEffectProbability;

    // Установить яркость свеения светодиода
    void hdSetLedBright(int index);

    void applyNewEffect();

    int ledMatrix[4] = {
            3, 5, 6, 9
    };

};


#endif //EFLAME328_EFLAME_H
