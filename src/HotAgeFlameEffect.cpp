//
// Created by dsporykhin on 29.02.20.
//

#include "HotAgeFlameEffect.h"

HotAgeFlameEffect::HotAgeFlameEffect(EFlame *eflame) : AbstractFlameEffect(0.1, eflame, true) {

}

void HotAgeFlameEffect::applyEffect() {
    int index = random(0, 3);
//    int bright = random(10, 20) + flame->brightMax - 30;
    int bright = flame->brightMax;
    int speed = flame->doubleRandom() * (flame->speedChangeBrightMax - flame->speedChangeBrightMin -2) + flame->speedChangeBrightMin + 2;
    double brightDelta = bright - flame->brights[index];
    flame->stepCountToDo = round(brightDelta / (double)speed);
    if (flame->stepCountToDo == 0) {
        flame->stepCountToDo = 1;
    }
    flame->brightDelta[index] = brightDelta / (double)flame->stepCountToDo;
#ifdef CON_DEBUG
    Serial.println("HotAgeFlameEffect  index=" + String(index) + " brgt=" + String(bright) + "   speed=" + String(speed) + "  totalDelta=" + String(brightDelta));
#endif
}