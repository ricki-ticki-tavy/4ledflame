//
// Created by dsporykhin on 29.02.20.
// randomly creates hot pixels on flame
//

#ifndef EFLAME328_HOTAGEFLAMEEFFECT_H
#define EFLAME328_HOTAGEFLAMEEFFECT_H


#include "EFlame.h"

class HotAgeFlameEffect: public AbstractFlameEffect{
public:
    HotAgeFlameEffect(EFlame* eflame);

    void applyEffect();
};


#endif //EFLAME328_HOTAGEFLAMEEFFECT_H
