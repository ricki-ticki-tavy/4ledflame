//
// Created by dsporykhin on 01.03.20.
//

#include <stdlib.h>
#include "NormalizeFlameFlameEffect.h"

NormalizeFlameFlameEffect::NormalizeFlameFlameEffect(EFlame *eflame) : AbstractFlameEffect(0.3, eflame, true) {

}

void NormalizeFlameFlameEffect::applyEffect() {
    long speed = random(flame->speedChangeBrightMin, flame->speedChangeBrightMax);

    double maxDelta = 0;

    for (int index = 0; index < 4; index++) {
        double newDelta = flame->doubleAbs(flame->normalBright - flame->brights[index]);
        maxDelta = max(maxDelta, newDelta);
    }
    flame->stepCountToDo = round(maxDelta / speed);
    flame->stepCountToDo = flame->stepCountToDo == 0 ? 1 : flame->stepCountToDo;

    for (int index = 0; index < 4; index++) {
        flame->brightDelta[index] =
                ((double) flame->normalBright - flame->brights[index]) / (double) flame->stepCountToDo;
    }
#ifdef CON_DEBUG
    Serial.println("NormalizeFlameFlameEffect   d0=" + String((int) flame->brightDelta[0])
                   + "  d1=" + String((int) flame->brightDelta[1])
                   + "  d2=" + String((int) flame->brightDelta[2])
                   + "  d3=" + String((int) flame->brightDelta[3]));
#endif
}