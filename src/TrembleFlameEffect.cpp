//
// Created by dsporykhin on 01.03.20.
//

#include "TrembleFlameEffect.h"

TrembleFlameEffect::TrembleFlameEffect(EFlame *eflame) : AbstractFlameEffect(0.1, eflame, true) {

}

void TrembleFlameEffect::applyEffect() {
    // выбрать направление смещения
    int direction = random(0, 7);

    // подберем скорость изменений
    double speed = flame->doubleRandom() * (flame->speedChangeBrightMax - flame->speedChangeBrightMin) +
                   flame->speedChangeBrightMin;

    // возьмем дельту к нормальной яркости. Туда и будем стремиться
    double newBrightDelta = flame->doubleRandom() * (flame->brightMax - flame->normalBright - TrembleFlameEffect_MIN_BRIGHT_CHANGE) + TrembleFlameEffect_MIN_BRIGHT_CHANGE;

    // найдем максимальную дельту для расчета числа шагов
    double maxDelta = 0;
    for (int index = 0; index < 4; index++) {
        double localDelta = flame->doubleAbs(
                flame->normalBright + newBrightDelta * matrix[direction][index] - flame->brights[index]);
        maxDelta = max(maxDelta, localDelta);
    }

    // получим кол-во шагов
    flame->stepCountToDo = round(maxDelta / speed);
    flame->stepCountToDo = flame->stepCountToDo == 0 ? 1 : flame->stepCountToDo;


    // расчитаем индивидуальные делты
    for (int index = 0; index < 4; index++) {
        flame->brightDelta[index] =
                (flame->normalBright + newBrightDelta * matrix[direction][index] - flame->brights[index]) /
                (double) flame->stepCountToDo;
    }
#ifdef CON_DEBUG
    Serial.println("TrembleFlameEffect  dir=" + String(direction));
#endif

}