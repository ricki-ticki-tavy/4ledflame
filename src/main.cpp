//
// Created by dsporykhin on 29.02.20.
//
#include <Arduino.h>
#include "EFlame.h"
#include "AvgBrightChangeFlameEffect.h"
#include "HotAgeFlameEffect.h"
#include "NormalizeFlameFlameEffect.h"
#include "TrembleFlameEffect.h"

EFlame* flame;

void setup(){
#ifdef CON_DEBUG
    Serial.begin(57600);
#endif

    flame = new EFlame();
    AbstractFlameEffect* effect = new AvgBrightChangeFlameEffect(flame);
    flame->appendEffect(effect);
    effect = new HotAgeFlameEffect(flame);
    flame->appendEffect(effect);
    effect = new NormalizeFlameFlameEffect(flame);
    flame->appendEffect(effect);
    effect = new TrembleFlameEffect(flame);
    flame->appendEffect(effect);
}

void loop(){
    flame->loop();
    delay(10);
}