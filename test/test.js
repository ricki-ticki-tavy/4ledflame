function runFlame() {
    const flame = new Flame();
    flame.addEffect(new FlameEffect("midBright", 0.3, true,
        function (flame) {
            flame.stepCountToDo = 0;
            flame.normalBright = 190 + ((Math.random() * 60) | 0 ) - 30;
        })
    );

    flame.addEffect(new FlameEffect("hotAge", 0.03, true,
        function (flame) {
            const index = Math.round(Math.random() * 3) | 0;
            // const bright = Math.random() * (flame.brightMax - flame.brightMin) + flame.brightMin;
            const bright = Math.random() * (30) + flame.brightMax -  30;
            const speed = Math.random() * (flame.speedChangeBrightMax - flame.speedChangeBrightMin) + flame.speedChangeBrightMin;
            const brightDelta = bright - flame.brights[index];
            flame.stepCountToDo = Math.floor(brightDelta / speed + 0.5) | 0;
            flame.brightDelta[index] = brightDelta / flame.stepCountToDo;
        })
    );

    flame.addEffect(new FlameEffect("normalize", 0.3, true,
        function (flame) {
            const speed = Math.random() * (flame.speedChangeBrightMax - flame.speedChangeBrightMin) + flame.speedChangeBrightMin;

            var maxDelta = 0;

            for (i = 0; i < 4; i++) {
                if (maxDelta < Math.abs(flame.normalBright - flame.brights[i])) {
                    maxDelta = Math.abs(flame.normalBright - flame.brights[i]);
                }
            }
            flame.stepCountToDo = Math.floor(maxDelta / speed + 0.5) | 0;

            if (flame.stepCountToDo > 0) {
                for (i = 0; i < 4; i++) {
                    flame.brightDelta[i] = (flame.normalBright - flame.brights[i]) / flame.stepCountToDo;
                }
            }
        })
    );

    flame.addEffect(new FlameEffect("direction", 0.05, true,
        function (flame) {

            const matrixArray = new Array(
                new Array(0.8, 0.8, -1.4, -1.4) // up
                , new Array(0.3, 1, 0.3, -2.5)    // up-right
                , new Array(-1.4, 0.8, 0.8, -1.4) // right
                , new Array(-2.5, 0.3, 1, 0.3)      // down-right
                , new Array(-1.4, -1.4, 0.8, 0.8) // down
                , new Array(0.3, -2.5, 0.3, 1) // down-left
                , new Array(0.8, -1.4, -1.4, 0.8) // left
                , new Array(1, 0.3, -2.5, 0.3) // up-left
            )

            // выбрать направление смещения
            const direction = Math.floor(Math.random() * 7 + 0.5) | 0;

            // выберем матрикс
            const matrix = matrixArray[direction];

            // подберем скорость изменений
            const speed = Math.random() * (flame.speedChangeBrightMax - flame.speedChangeBrightMin) + flame.speedChangeBrightMin;

            // возьмем дельту к нормальной яркости. Туда и будем стремиться
            const newBrightDelta = Math.random() * (flame.brightMax - flame.normalBright);

            // найдем максимальную дельту для расчета числа шагов
            var maxDelta = 0;
            for (i = 0; i < 4; i++) {
                const localDelta = flame.normalBright + newBrightDelta * matrix[i] - flame.brights[i];
                if (maxDelta < Math.abs(localDelta)) {
                    maxDelta = Math.abs(localDelta);
                }
            }

            // получим кол-во шагов
            flame.stepCountToDo = Math.floor(maxDelta / speed + 0.5) | 0;

            // расчитаем индивидуальные делты
            if (flame.stepCountToDo > 0) {
                for (i = 0; i < 4; i++) {
                    flame.brightDelta[i] = (flame.normalBright + newBrightDelta * matrix[i] - flame.brights[i]) / flame.stepCountToDo;
                }
            }
        })
    );

    flame.run();

};


FlameEffect = function (name, probability, preventPriorEffect, action) {
    this.probability = probability; // вероятность эффекта
    this.action = action.bind(this);
    this.name = name;
    this.preventPriorEffect = preventPriorEffect;
};
FlameEffect.prototype = {
    constructor: FlameEffect,
    apply: function (flame) {
        if (this.preventPriorEffect) {
            flame.brightDelta[0] = 0;
            flame.brightDelta[1] = 0;
            flame.brightDelta[2] = 0;
            flame.brightDelta[3] = 0;
        }
        this.action(flame);
    }
};

Flame = function () {
    this.divs = new Array(
        document.getElementById("part_1")
        , document.getElementById("part_2")
        , document.getElementById("part_3")
        , document.getElementById("part_4"));

    this.speedChangeBrightMin = 1;
    this.speedChangeBrightMax = 10;

    this.brightMin = 100;
    this.brightMax = 255;
    this.normalBright = 190;

    this.probabilityChangeAction = 0.000000001;

    this.brights = new Array(190, 190, 190, 190);
    this.brightDelta = new Array(0, 0, 0, 0);
    this.stepCountToDo = 0;

    this.effects = new Array();
    this.totalEffectProbability = 0;

};

Flame.prototype = {
    constructor: Flame,

    __hdSetLedBright__: function (lightIndex) {
        var col = (this.brights[lightIndex] | 0).toString(16);
        if (col.length < 2) {
            col = "0" + col;
        }
        this.divs[lightIndex].style.backgroundColor = "#" + col + col + col;
    },

    __processPartFlame: function (lightIndex) {
        this.brights[lightIndex] += this.brightDelta[lightIndex];
        this.__hdSetLedBright__(lightIndex);
    },

    addEffect: function (effect) {
        this.effects.push(effect);
        this.totalEffectProbability = 0;
        this.effects.forEach(function (effect) {
            this.totalEffectProbability += effect.probability;
        }.bind(this));
    },


    run: function () {
        this.timeout = window.setInterval(this.loopHandler.bind(this), 20);
    },

    __applyNewEffect: function () {
        var effectSelector = Math.random() * this.totalEffectProbability;
        const newEffect = this.effects.find(function (effect, index) {
            if (effect.probability > effectSelector) {
                return true;
            } else {
                effectSelector -= effect.probability;
                return false;
            }
        }.bind(this));
        newEffect.apply(this);
    },

    loopHandler: function () {
        if (this.stepCountToDo <= 0 || Math.random() <= this.probabilityChangeAction) {
            // меняем направление свечения и все такое
            if (this.stepCountToDo > 0    // именно так! если он больше, но мы тут, то сработала безусловная смена эффекта
                || Math.random() <= this.totalEffectProbability) {
                this.__applyNewEffect();
            }
        } else {
            for (i = 0; i < 4; i++) {
                this.__processPartFlame(i);
            }
            this.stepCountToDo--;
        }
    }
}